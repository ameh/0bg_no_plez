## Henlo

It's pretty easy to run it, just delete everything inside the hello_world example directory, and git clone this.

Then go to build/ and make buildroot. 

Since everything is already linked, it'll make it much faster to compile it rather than doing it all by itself in a different directory.

Then just start the trustzone VM using make run-only, run the hello_world_application but instead it'll output what we need for this, without having to recompile the entire thing again
as object files linking the two are already there.
